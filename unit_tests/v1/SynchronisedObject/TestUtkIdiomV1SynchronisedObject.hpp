#ifndef UNIT_TESTS_V1_SYNCHRONISEDOBJECT_TESTUTKIDIOMV1SYNCHRONISEDOBJECT_HPP
#define UNIT_TESTS_V1_SYNCHRONISEDOBJECT_TESTUTKIDIOMV1SYNCHRONISEDOBJECT_HPP


#include <mutex>

#include <boost/thread/lockable_adapter.hpp>

#include <gtest/gtest.h>

#include <utk/idiom/SynchronisedObject.hpp>


TEST (TestUtkIdiomV1SynchronisedObject, EmptyTest) {
}


TEST (TestUtkIdiomV1SynchronisedObject, StdMutexLockable) {
	struct Test {};

	utk::idiom::
	    SynchronisedObject< Test, boost::lockable_adapter< std::mutex > >
	        lockable_test{};

	lockable_test.lock ();
	lockable_test.unlock ();

	if (lockable_test.try_lock ()) {
		lockable_test.unlock ();
	}
}


TEST (TestUtkIdiomV1SynchronisedObject, CopyConstructFromBaseType) {
	struct Test {
		Test () = default;

		Test (const Test& i_src)
		    : initialised_from_copy_constructor{true} {
		}


		bool initialised_from_copy_constructor = false;
	};

	Test test_src;

	utk::idiom::
	    SynchronisedObject< Test, boost::lockable_adapter< std::mutex > >
	        lockable_test{test_src};

	ASSERT_TRUE (lockable_test.initialised_from_copy_constructor)
	    << "The object should have been initialised by copy constructor.";
}


TEST (TestUtkIdiomV1SynchronisedObject, MoveConstructFromBaseType) {
	struct Test {
		Test () = default;

		Test (Test&& i_src)
		    : initialised_from_move_constructor{true} {
			i_src.moved_by_move_constructor = true;
		}


		bool initialised_from_move_constructor = false;
		bool moved_by_move_constructor = false;
	};

	Test test_src;

	utk::idiom::
	    SynchronisedObject< Test, boost::lockable_adapter< std::mutex > >
	        lockable_test{std::move (test_src)};

	ASSERT_TRUE (lockable_test.initialised_from_move_constructor)
	    << "The object should have been initialised by move constructor.";

	ASSERT_TRUE (test_src.moved_by_move_constructor)
	    << "The object should have been moved by move constructor.";
}


#endif // UNIT_TESTS_V1_SYNCHRONISEDOBJECT_TESTUTKIDIOMV1SYNCHRONISEDOBJECT_HPP
