# Copyright 2019-2021 Utility Tool Kit Open Source Contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License.  You may obtain a copy of
# the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
# License for the specific language governing permissions and limitations under
# the License.
#
# File name: utk.idiom/CMakeLists.txt
#
# Description: utk.idiom library dependencies.


#####################
# Find dependencies #
#####################
function (utk_idiom_find_dependencies)
  find_package (Boost 1.53 REQUIRED
    COMPONENTS
    thread
    )
endfunction (utk_idiom_find_dependencies)


####################
# Use dependencies #
####################
function (utk_idiom_use_dependencies)
  set (_options
    )
  set (_multi_value_args
    TARGET
    )
  set (_one_value_args
    ""
    )

  cmake_parse_arguments (i
    "${_options}" "${_one_value_args}" "${_multi_value_args}" ${ARGN})

  if (NOT i_TARGET)
    message (SEND_ERROR "Provide IMPORTED_TARGET or INTERFACE_TARGET argument")
  endif ()

  foreach (_target IN LISTS i_TARGET)
    target_link_libraries (
      ${_target}
      INTERFACE
      Boost::thread
      )
  endforeach (_target IN LISTS _interface_targets)
endfunction (utk_idiom_use_dependencies)
