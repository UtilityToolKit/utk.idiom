// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/Pimpl_impl.hpp
//
// Description: Provides definition of the PIMPL holder class.


#ifndef INCLUDE_UTK_IDIOM_PIMPL_IMPL_HPP
#define INCLUDE_UTK_IDIOM_PIMPL_IMPL_HPP


#include <utility>

#include <utk/idiom/Pimpl.hpp>


UTK_IDIOM_NAMESPACE_OPEN


template < typename T >
Pimpl< T >::Pimpl ()
    : pimpl_{std::make_unique< T > ()} {
}


template < typename T >
Pimpl< T >::Pimpl (Pimpl< T >&& i_other)
    : pimpl_{std::move (i_other.pimpl_)} {
}


template < typename T >
template < typename... Args >
Pimpl< T >::Pimpl (Args&&... args)
    : pimpl_{std::make_unique< T > (std::forward< Args > (args)...)} {
}


template < typename T >
Pimpl< T >::~Pimpl () {
}


template < typename T >
T* Pimpl< T >::operator-> () {
	return pimpl_.get ();
}


template < typename T >
T& Pimpl< T >::operator* () {
	return *pimpl_.get ();
}


template < typename T >
const T* Pimpl< T >::operator-> () const {
	return pimpl_.get ();
}


template < typename T >
const T& Pimpl< T >::operator* () const {
	return *pimpl_.get ();
}


template < typename T >
Pimpl< T >& Pimpl< T >::operator= (Pimpl< T >&& i_src) {
	pimpl_ = ::std::move (i_src.pimpl_);

	return *this;
}


UTK_IDIOM_NAMESPACE_CLOSE


#endif /* INCLUDE_UTK_IDIOM_PIMPL_IMPL_HPP */
