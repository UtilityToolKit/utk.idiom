// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/state_stack/ApplyState.hpp
//
// Description: Provides definition of a function for applying the state stored
//              in the given state vector.


#ifndef UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_APPLYSTATE_HPP
#define UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_APPLYSTATE_HPP


#include <tuple>
#include <utility>

#include "utk/idiom/namespace.hpp"


UTK_IDIOM_NAMESPACE_OPEN


namespace detail {
	template <
	    class StateVector_,
	    class ApplyState_,
	    ::std::size_t... kStateIndex_ >
	void ApplyState (
	    ApplyState_&& i_apply_state,
	    StateVector_&& i_state_vector,
	    ::std::index_sequence< kStateIndex_... >) {
		// https://stackoverflow.com/a/20441189/1292814
		::std::forward< ApplyState_ > (i_apply_state) (
		    ::std::get< kStateIndex_ > (
		        ::std::forward< StateVector_ > (i_state_vector))...);
	}
} // namespace detail


template < class ApplyState_, class StateVector_ >
void ApplyState (ApplyState_&& i_apply_state, StateVector_&& i_state_vector) {
	using StateVectorSize = ::std::tuple_size<
	    typename ::std::remove_reference< StateVector_ >::type >;

	detail::ApplyState (
	    ::std::forward< ApplyState_ > (i_apply_state),
	    ::std::forward< StateVector_ > (i_state_vector),
	    ::std::make_index_sequence< StateVectorSize::value > ());
}


UTK_IDIOM_NAMESPACE_CLOSE


#endif /* UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_APPLYSTATE_HPP */
