// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/state_stack/StoreState.hpp
//
// Description: Provides definition of a function for saving state into the
//              given state vestor.


#ifndef UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_SAVESTATE_HPP
#define UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_SAVESTATE_HPP


#include <tuple>
#include <utility>

#include "utk/idiom/namespace.hpp"


UTK_IDIOM_NAMESPACE_OPEN


namespace detail {
	template <
	    class StateVector_,
	    class RetreiveState_,
	    class... Tags_,
	    ::std::size_t... kStateIndex_ >
	void SaveState (
	    StateVector_&& o_state_vector,
	    RetreiveState_&& i_retreive_state,
	    ::std::index_sequence< kStateIndex_... >,
	    Tags_&&... i_tags) {
		// https://stackoverflow.com/a/39412496/1292814
		int _[] = {0,
		           (std::forward< RetreiveState_ > (i_retreive_state) (
		                std::forward< Tags_ > (i_tags),
		                std::get< kStateIndex_ > (
		                    std::forward< StateVector_ > (o_state_vector))),
		            0)...};
		(void)_;
	}
} // namespace detail


template < class StateVector_, class RetreiveState_, class... Tags_ >
void SaveState (
    StateVector_&& o_state_vector,
    RetreiveState_&& i_retreive_state,
    Tags_... i_tags) {
	using StateVectorSize = ::std::tuple_size<
	    typename ::std::remove_reference< StateVector_ >::type >;

	static_assert (
	    StateVectorSize::value == sizeof...(i_tags),
	    "State vector size must match tag list length");

	detail::SaveState (
	    ::std::forward< StateVector_ > (o_state_vector),
	    ::std::forward< RetreiveState_ > (i_retreive_state),
	    ::std::index_sequence_for< Tags_... >{},
	    ::std::forward< Tags_ > (i_tags)...);
}


UTK_IDIOM_NAMESPACE_CLOSE


#endif /* UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_SAVESTATE_HPP */
