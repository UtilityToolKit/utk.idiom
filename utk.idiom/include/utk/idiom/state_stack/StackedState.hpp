// Copyright 2019-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/state_stack/ApplyState.hpp
//
// Description: Provides a definition of a function for creating an object of an
//              RAII class that will store the state into a state vector using
//              the given state retrieval functions and will restore the state
//              upon destruction using the given state restoration functions.


#ifndef UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_STACKEDSTATE_HPP
#define UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_STACKEDSTATE_HPP


#include <array>
#include <utility>

#include "utk/idiom/namespace.hpp"

#include "utk/idiom/state_stack/ApplyState.hpp"
#include "utk/idiom/state_stack/SaveState.hpp"


UTK_IDIOM_NAMESPACE_OPEN


namespace detail {
	template <
	    class StateType_,
	    class ApplyState_,
	    ::std::size_t StateVectorSize_ >
	class StackedState {
	public:
		template < class RetreiveState_, class... Tags_ >
		explicit StackedState (
		    RetreiveState_&& i_retreive_state,
		    ApplyState_&& i_apply_state,
		    Tags_&&... i_tags)
		    : apply_state_{::std::forward< ApplyState_ > (i_apply_state)} {
			idiom::SaveState (
			    state_vector_,
			    ::std::forward< RetreiveState_ > (i_retreive_state),
			    ::std::forward< Tags_ > (i_tags)...);
		}


		~StackedState () {
			idiom::ApplyState (apply_state_, state_vector_);
		}


	private:
		using StateVector = ::std::array< StateType_, StateVectorSize_ >;


		StateVector state_vector_;
		ApplyState_ apply_state_;
	};


	template <
	    class StateType_,
	    class RetreiveState_,
	    class ApplyState_,
	    ::std::size_t... kIndices_ >
	auto StackState (
	    RetreiveState_&& i_retreive_state,
	    ApplyState_&& i_apply_state,
	    ::std::index_sequence< kIndices_... >) {
		return StackedState< StateType_, ApplyState_, sizeof...(kIndices_) >{
		    std::forward< RetreiveState_ > (i_retreive_state),
		    std::forward< ApplyState_ > (i_apply_state),
		    kIndices_...};
	}
} // namespace detail


template <
    class StateType_,
    class RetreiveState_,
    class ApplyState_,
    class... Tags_ >
auto StackState (
    RetreiveState_&& i_retreive_state,
    ApplyState_&& i_apply_state,
    Tags_&&... i_tags) {
	return detail::StackedState< StateType_, ApplyState_, sizeof...(i_tags) >{
	    ::std::forward< RetreiveState_ > (i_retreive_state),
	    ::std::forward< ApplyState_ > (i_apply_state),
	    ::std::forward< Tags_ > (i_tags)...};
}


template <
    class StateType_,
    size_t kStateVectorSize_,
    class RetreiveState_,
    class ApplyState_ >
auto StackState (
    RetreiveState_&& i_retreive_state, ApplyState_&& i_apply_state) {
	return detail::StackState< StateType_ > (
	    ::std::forward< RetreiveState_ > (i_retreive_state),
	    ::std::forward< ApplyState_ > (i_apply_state),
	    ::std::make_index_sequence< kStateVectorSize_ > ());
}


UTK_IDIOM_NAMESPACE_CLOSE


#endif /* UTK_IDIOM_INCLUDE_UTK_IDIOM_STATE_STACK_STACKEDSTATE_HPP */
