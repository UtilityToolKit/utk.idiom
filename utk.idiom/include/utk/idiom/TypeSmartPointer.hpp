// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/TypeSpartPointer.hpp
//
// Description: Provides macros for defining smart pointer types and smart
//              pointer creation functions for specific classes.


#ifndef INCLUDE_UTK_IDIOM_TYPESMARTPOINTER_HPP
#define INCLUDE_UTK_IDIOM_TYPESMARTPOINTER_HPP


#include <memory>


#define UTK_IDIOM_SHARED_PTR(TYPE_NAME)                                        \
	using SharedPtr = std::shared_ptr< TYPE_NAME >;

#define UTK_IDIOM_MAKE_SHARED(TYPE_NAME)                                       \
	template < typename... Args >                                              \
	static auto MakeShared (Args&&... i_args)                                  \
	    ->decltype (std::make_shared< TYPE_NAME > (                            \
	        std::forward< Args > (i_args)...)) {                               \
		return std::make_shared< TYPE_NAME > (                                 \
		    std::forward< Args > (i_args)...);                                 \
	};


#define UTK_IDIOM_UNIQUE_PTR(TYPE_NAME)                                        \
	using UniquePtr = std::unique_ptr< TYPE_NAME >;

#define UTK_IDIOM_MAKE_UNIQUE(TYPE_NAME)                                       \
	template < typename... Args >                                              \
	static auto MakeUnique (Args&&... i_args)                                  \
	    ->decltype (std::make_unique< TYPE_NAME > (                            \
	        std::forward< Args > (i_args)...)) {                               \
		return std::make_unique< TYPE_NAME > (                                 \
		    std::forward< Args > (i_args)...);                                 \
	};

#define UTK_IDIOM_WEAK_PTR(TYPE_NAME)                                          \
	using WeakPtr = std::weak_ptr< TYPE_NAME >;


#endif /* INCLUDE_UTK_IDIOM_TYPESMARTPOINTER_HPP */
