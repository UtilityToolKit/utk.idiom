// Copyright 2018-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/SynchronisedObject.hpp
//
// Description: Provides declaration for a generic wrapper to make objects of
//              the given type lockable to synchronise multithreaded access.


#ifndef INCLUDE_UTK_IDIOM_SYNCHRONISEDOBJECT_HPP
#define INCLUDE_UTK_IDIOM_SYNCHRONISEDOBJECT_HPP


#ifndef BOOST_CHRONO_DONT_PROVIDE_HYBRID_ERROR_HANDLING
/*
  Boost.Thread lockable concepts use Boost.Chrono to perform some
  checks. Boost.Chrono itself is not header-only by default. To make it header
  only two steps are required: define BOOST_CHRONO_HEADER_ONLY, which is pretty
  self-explanatory, and define BOOST_CHRONO_DONT_PROVIDE_HYBRID_ERROR_HANDLING
  to disable hybrid error handling that requires linking Boost.System.
*/
#	ifndef BOOST_CHRONO_HEADER_ONLY
#		define BOOST_CHRONO_HEADER_ONLY
#		define UTK_IDIOM_BOOST_CHRONO_HEADER_ONLY_DEFINED
#	endif // BOOST_CHRONO_HEADER_ONLY
#	define BOOST_CHRONO_DONT_PROVIDE_HYBRID_ERROR_HANDLING

#	include <boost/thread/lockable_concepts.hpp>

#	ifdef UTK_IDIOM_BOOST_CHRONO_HEADER_ONLY_DEFINED
#		undef BOOST_CHRONO_HEADER_ONLY
#		undef UTK_IDIOM_BOOST_CHRONO_HEADER_ONLY_DEFINED
#	endif // BOOST_CHRONO_HEADER_ONLY

#	undef BOOST_CHRONO_DONT_PROVIDE_HYBRID_ERROR_HANDLING
#endif

#include <boost/concept/requires.hpp>

#include "utk/idiom/namespace.hpp"

#include "utk/idiom/TypeSmartPointer.hpp"


UTK_IDIOM_NAMESPACE_OPEN


/**
   @brief A generic wrapper to make objects of the given type lockable to
   synchronise multithreaded access

   @details The motivation for this class is to enable in-place declaration of
   the synchronised variables of the given type.

   The class requires the LockableAdapter to satisfy requirements of at least
   BasicLockable concept.

   The objects of type SynchronisedObject< Type, LockableAdapter > are neither
   copyable nor movable. This is due to lack of ability to safely copy or move
   an object that may be locked: if the object is already locked by the thread
   that requests "copy" or "move" operation, the locking of the object leads to
   Undefined Behaviour; if the object is not locked during the operation, then
   its state is either undefined in case of "copy" operation or may be
   invalidated by the "move" operation if it is in use by another thread.

   The class provides a set of smart pointer classes and smart pointer creation
   functions to ease the use of the uncopiable and unmovable objects.
*/
template <
    class Type,
    class LockableAdapter,
    typename =
        typename std::enable_if< !std::is_fundamental< Type >::value >::type,
    typename = BOOST_CONCEPT_REQUIRES (
        ((boost::BasicLockable< LockableAdapter >)), (void)) >
class SynchronisedObject : public Type, public LockableAdapter {
public:
	using SynchronisedObjectType = SynchronisedObject< Type, LockableAdapter >;

	UTK_IDIOM_SHARED_PTR (SynchronisedObjectType);
	UTK_IDIOM_UNIQUE_PTR (SynchronisedObjectType);
	UTK_IDIOM_WEAK_PTR (SynchronisedObjectType);

	UTK_IDIOM_MAKE_SHARED (SynchronisedObjectType);
	UTK_IDIOM_MAKE_UNIQUE (SynchronisedObjectType);

	using Type::Type;

	SynchronisedObject (const Type& i_src)
	    : Type{i_src}
	    , LockableAdapter{} {
	}

	SynchronisedObject (Type&& i_src)
	    : Type{std::move (i_src)}
	    , LockableAdapter{} {
	}
};


UTK_IDIOM_NAMESPACE_CLOSE


#endif /* INCLUDE_UTK_IDIOM_SYNCHRONISEDOBJECT_HPP */
