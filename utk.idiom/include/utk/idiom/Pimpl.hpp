// Copyright 2017-2021 Utility Tool Kit Open Source Contributors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License.  You may obtain a copy
// of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
// License for the specific language governing permissions and limitations under
// the License.
//
// Author: Innokentiy Alaytsev <alaitsev@gmail.com>
//
// File name: utk.idiom/include/utk/idiom/Pimpl.hpp
//
// Description: Provides definition of the PIMPL holder class.


#ifndef INCLUDE_UTK_IDIOM_PIMPL_HPP
#define INCLUDE_UTK_IDIOM_PIMPL_HPP


#include <memory>

#include "utk/idiom/namespace.hpp"


UTK_IDIOM_NAMESPACE_OPEN


/**
   @note Based on Herb Sutter pimpl implementation from GotW #101, Part 2
   https://herbsutter.com/gotw/_101/
*/
template < typename T >
class Pimpl {
public:
	Pimpl ();
	Pimpl (Pimpl&& i_other);

	template < typename... Args >
	explicit Pimpl (Args&&...);

	~Pimpl ();

	T* operator-> ();
	const T* operator-> () const;

	T& operator* ();
	const T& operator* () const;

	Pimpl< T >& operator= (Pimpl< T >&& i_src);


private:
	std::unique_ptr< T > pimpl_;
};


UTK_IDIOM_NAMESPACE_CLOSE


#endif /* INCLUDE_UTK_IDIOM_PIMPL_HPP */
