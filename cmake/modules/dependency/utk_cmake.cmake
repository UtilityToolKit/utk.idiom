include (FetchContent)


FetchContent_Declare (
  utk_cmake
  GIT_REPOSITORY https://gitlab.com/UtilityToolKit/utk.cmake.git
  GIT_TAG        v1.0.0
  GIT_PROGRESS   TRUE
  GIT_SHALLOW    TRUE
  )

FetchContent_GetProperties (utk_cmake)

if (NOT utk_cmake_POPULATED)
  FetchContent_Populate (utk_cmake)
endif()

if (utk_cmake_POPULATED)
  set (CMAKE_MODULE_PATH
    ${CMAKE_MODULE_PATH}
    "${utk_cmake_SOURCE_DIR}"
    )

  set (UTK_CMAKE_MODULE_DIR "${utk_cmake_SOURCE_DIR}/utk.cmake")
endif()
