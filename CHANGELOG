Version 0.4.0 (2021-07-17)

    New features:

    * Update to utk.cmake v1.0.0.


    Fixed problems:

    * Prevent setting from leaking from this project as subdirectory.

    * Disabled useless CMake policy.


Version 0.3.0 (2019-09-13)

    New features:

    * Implemented moving assignment operator for Pimpl class.

    * Implemented functions and type for manupulating the state through RAII.

    * Introduced macros for opening and closing the library namespace.


    Fixed problems:

    * Fixed missing universal references for smart pointers factory functions.


Version 0.2.2 (2018-10-31)

    New features:

    * utk.cmake v0.7.3.


    Fixed problems:

    * Fixed handling of the CMake package dependencies.


Version 0.2.1 (2018-06-28)

    New features:

    * utk.cmake v0.7.0 for better dependency handling support.

    * Introduced an option to control development files intallation.


    Fixed problems:

    * Fixed project declaration placement.

    * Removed useless commented code.

    * Fixed the use of a non-existent variable in *-target-config.cmake
      template.

    * Improved dependency handling when built as subproject.


Version 0.2.0 (2018-06-24)

    New features:

    * Header only library nowm

    * utk.cmake powered build system.

    * utk::idiom::SynchronisedObject class for in-place creation of lockable
      objects.


Version 0.1.0 (2018-05-16)

    The first version.

    * Implementation of PIMPL idiom class.

    * A set of macros for generating smart pointer type declarations.
